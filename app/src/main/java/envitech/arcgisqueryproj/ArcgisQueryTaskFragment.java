package envitech.arcgisqueryproj;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapView;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Feature;
import com.esri.core.map.FeatureResult;
import com.esri.core.map.Graphic;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.esri.core.symbol.SimpleFillSymbol;
import com.esri.core.tasks.query.QueryParameters;
import com.esri.core.tasks.query.QueryTask;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ArcgisQueryTaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArcgisQueryTaskFragment extends Fragment {

	public static final String TAG = "ArcgisQuery";

	private MapView mMapView;
	GraphicsLayer areaLayer;
	ArrayList<Integer> areasUidList = new ArrayList<Integer>();

	public ArcgisQueryTaskFragment() {
	}

	public static ArcgisQueryTaskFragment newInstance() {
		ArcgisQueryTaskFragment fragment = new ArcgisQueryTaskFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_arcgis_query_task, container, false);
		mMapView = (MapView) view.findViewById(R.id.map);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(true);

		//todo mark these 2 rows to see the problem
		LicenseResult licenseResult = ArcGISRuntime.setClientId("jHFHqdhQa6CcuO0s");
		Log.e(TAG, "licenseResult="+licenseResult);
		LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();
		Log.e(TAG, "licenseLevel="+licenseLevel);

		mMapView.enableWrapAround(true);
		mMapView.setOnStatusChangedListener(new OnStatusChangedListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void onStatusChanged(Object source, STATUS status) {
				if (source == mMapView && status == STATUS.INITIALIZED) {
					Log.e(TAG, "INITIALIZED: "+ source.getClass().getSimpleName());
					Envelope envelope = new Envelope(42.504503, -92.790527, 47.264320, -86.330566);
					areaLayer = new GraphicsLayer(SpatialReference.create(SpatialReference.WKID_WGS84), envelope);
					//areaLayer = new GraphicsLayer(SpatialReference.create(SpatialReference.WKID_WGS84), envelope , GraphicsLayer.RenderingMode.DYNAMIC);
					areaLayer.setOpacity(0.5f);
					areaLayer.setName("areaLayer");
					areaLayer.setVisible(true);
					mMapView.addLayer(areaLayer);
					new AsyncQueryTaskWisconsin().execute();
				} else if (source == mMapView && status == STATUS.INITIALIZATION_FAILED) {
					Log.e(TAG, "INITIALIZATION_FAILED: "+ status.getError().getDescription());
				} else if ( status == STATUS.LAYER_LOADED) {
					Log.e(TAG, "LAYER_LOADED: "+ source);
				} else if ( status == STATUS.LAYER_LOADING_FAILED) {
					Log.e(TAG, "LAYER_LOADED_ERROR:source "+ source+ "  status:"+ status.getError().getDescription());
				}
			}
		});

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.arcgis_query_task_activity_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}


	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.wisconsin :{
				new AsyncQueryTaskWisconsin().execute();
				return true;
			}

			case R.id.reset:
				areaLayer.removeAll();
				return true;

		}
		return super.onOptionsItemSelected(item);
	}

	private class AsyncQueryTaskWisconsin extends AsyncTask<String, Void, FeatureResult> {
		ProgressDialog progress;
		@Override
		protected void onPreExecute() {
			progress = new ProgressDialog(getActivity());
			progress = ProgressDialog.show(getActivity(), "", "Please wait....query WisconsinAreas");
		}

		@Override
		protected FeatureResult doInBackground(String... queryArray) {
			//http://services.arcgisonline.com/ArcGIS/rest/services/Demographics/USA_Average_Household_Size/MapServer
			String queryLayerWisconsin = "http://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Demographics/ESRI_Census_USA/MapServer";
			String url = queryLayerWisconsin.concat("/3");
			QueryParameters qParameters = new QueryParameters();
			SpatialReference sr = SpatialReference.create(SpatialReference.WKID_WGS84_WEB_MERCATOR_AUXILIARY_SPHERE);
			qParameters.setOutSpatialReference(sr);
			qParameters.setReturnGeometry(true);
			String whereClause = "STATE_NAME='Wisconsin'";
			qParameters.setWhere(whereClause);
			String str[] = {"Name"};
			qParameters.setOutFields(str);

			QueryTask queryTask = new QueryTask(url);
			try {
				FeatureResult results = queryTask.execute(qParameters);
				return results;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(FeatureResult results) {
			String message = "No result comes back";

			if (results != null) {
				areaLayer.setVisible(true);

				Envelope extent = new Envelope();
				int size = (int) results.featureCount();
				int i=0;
				for (Object element : results) {
					progress.incrementProgressBy(size / 100);
					if (element instanceof Feature) {
						Feature feature = (Feature) element;
						int color =0;
						switch (i) {
							case 0:
								color= Color.YELLOW;
								i=1;
								break;
							case 1:
								color= Color.BLUE;
								i=0;
								break;
							default:
								break;
						}

						Graphic graphic = new Graphic(feature.getGeometry(), new SimpleFillSymbol(color), feature.getAttributes());

						Envelope envelope = new Envelope();
						graphic.getGeometry().queryEnvelope(envelope);
						extent.merge(envelope);

						areasUidList.add( areaLayer.addGraphic(graphic) );
					}
				}

				mMapView.setExtent(extent, 50);

				message = String.valueOf(results.featureCount())+ " WisconsinAreas.";
			}
			progress.dismiss();
			Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
		}
	}
}
