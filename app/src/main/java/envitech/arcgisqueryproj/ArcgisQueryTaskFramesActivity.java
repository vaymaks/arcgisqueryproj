package envitech.arcgisqueryproj;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ArcgisQueryTaskFramesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_arcgis_query_task_frames);

        setContentView(R.layout.activity_arcgis_query_task_frames);
        FragmentManager frgManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_arcgis_query_task_frames_container, ArcgisQueryTaskFragment.newInstance(), ArcgisQueryTaskFragment.TAG);
        fragmentTransaction.commit();
    }
}
